#!/usr/bin/env bash
PKG_NAME="pytest-freezegun"
WORKING_DIR=${HOME}/packaging-deb-${PKG_NAME}
# apply dfsg based on debian policy since I modified the real upstream (see below)
UPSTREAM_VERSION="0.4.2"
UPSTREAM_FOR_DEB_VERSION_SUFFIX="dfsg"
PKG_DEBMAKE_WORKING_DIR_NAME=${PKG_NAME}-${UPSTREAM_VERSION}+${UPSTREAM_FOR_DEB_VERSION_SUFFIX}
PKG_DEBMAKE_WORKING_DIR=${WORKING_DIR}/${PKG_DEBMAKE_WORKING_DIR_NAME}
UPSTREAM_GIT_REPO_URL=https://github.com/ktosiek/pytest-freezegun.git


set -x

mkdir -p "${WORKING_DIR}"

# --no-checkout: do not checkout to HEAD after cloning. that said, you won't see the source
# -o a.k.a. --origin: use foo-bar as the remote name (default is origin)
git clone --no-checkout -o upstream ${UPSTREAM_GIT_REPO_URL} "${PKG_DEBMAKE_WORKING_DIR}"

pushd "${PKG_DEBMAKE_WORKING_DIR}"

git checkout -b debian/master ${UPSTREAM_VERSION}

# dh_make version is very important since it takes care all other tools, libs... in the same distribution
dh_make --version
dh_make --createorig --python --yes

# strip unnecessary files to prepare files of debian template
rm -rf ./debian/source
rm -f ./debian/python-pytest-freezegun-doc.docs
rm -f ./debian/README.Debian
rm -f ./debian/README.source
echo "edit ./debian/control to (temporarily) remove doc build session until we provide it correctly"

tar -zcf ./debian.tar.gz ./debian
mv ./debian.tar.gz /tmp/
echo
echo "copy and edit /tmp/debian.tag.gz for your packaging source!"

popd

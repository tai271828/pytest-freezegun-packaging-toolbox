#!/usr/bin/env bash
PKG_NAME="pytest-freezegun"
WORKING_DIR=${HOME}/packaging-deb-${PKG_NAME}
UPSTREAM_VERSION="0.4.2"
PKG_SUFFIX="1"
PKG_VERSION=${UPSTREAM_VERSION}-${PKG_SUFFIX}
PKG_DEBMAKE_WORKING_DIR_NAME=${PKG_NAME}-${UPSTREAM_VERSION}
PKG_DEBMAKE_WORKING_DIR=${WORKING_DIR}/${PKG_DEBMAKE_WORKING_DIR_NAME}
UPSTREAM_GIT_REPO_URL=https://github.com/ktosiek/pytest-freezegun.git
BRANCH_DEB_MASTER="debian/master"


set -x

mkdir -p "${WORKING_DIR}"

# --no-checkout: do not checkout to HEAD after cloning. that said, you won't see the source
# -o a.k.a. --origin: use foo-bar as the remote name (default is origin)
git clone --no-checkout -o upstream ${UPSTREAM_GIT_REPO_URL} "${PKG_DEBMAKE_WORKING_DIR}"

pushd "${PKG_DEBMAKE_WORKING_DIR}"

git checkout ${UPSTREAM_VERSION}
git tag upstream/${UPSTREAM_VERSION}

git checkout -b ${BRANCH_DEB_MASTER} ${UPSTREAM_VERSION}

# dh_make version is very important since it takes care all other tools, libs... in the same distribution
dh_make --version
dh_make --createorig --python --yes -t "${HOME}"/toolbox/scripts/packaging-deb-${PKG_NAME}/debian

# TODO not sure why not executable
chmod +x debian/rules

realpath ./debian
git add ./debian
git commit -m "debianize the upstream source"

git tag debian/${PKG_VERSION}

gbp buildpackage --git-builder=sbuild \
    --git-pristine-tar --git-pristine-tar-commit \
    --git-debian-branch=${BRANCH_DEB_MASTER}

popd

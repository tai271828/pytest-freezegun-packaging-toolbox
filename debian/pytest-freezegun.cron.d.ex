#
# Regular cron jobs for the pytest-freezegun package
#
0 4	* * *	root	[ -x /usr/bin/pytest-freezegun_maintenance ] && /usr/bin/pytest-freezegun_maintenance
